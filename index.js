console.log("This is a message from the demo package");

const { spawn } = require('child_process')
console.log(spawn)

// Запустить команду "npm install"
const install = spawn('npm', ['install', 'https://gitlab.com/JuliaSS/vue2-leaflet-markercluster.git', 'https://gitlab.com/JuliaSS/vue-leaflet.git', 
'https://gitlab.com/JuliaSS/leaflet-test.git']);

// Обработка событий вывода для команды "npm install"
install.stdout.on('data', (data) => {
  console.log(`stdout: ${data}`);
});

install.stderr.on('data', (data) => {
  console.error(`stderr: ${data}`);
});

install.on('close', (code) => {
  console.log(`child process exited with code ${code}`);
});